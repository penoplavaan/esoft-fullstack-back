# Тестовое задание для ESOFT. BACKEND
Техническое описание:
>- Сервер **Express.js,**
>- База данных **PostgreSQL,**
>- Содержит два GET-endpoint: :
>>- [/api](http://penoplavaan.space:8080/api/) - Отдаёт объекты из базы данных списком,
>>- [/api/filtered](http://penoplavaan.space:8080/api/filtered) - Отдаёт данные с фильтрацией по введенным параметрам (выдаст данные только при наличии необходимых параметров в теле запроса).


Базовое взаимодействие:
[![](https://mermaid.ink/img/pako:eNqlkT1vwkAMhv-K5bGCsN_AQlN1oF9JpS63WDkDoeQuvXMEFeK_11FDAwtLt9M9j_y-so9YBcdoMPFXx77i-5rWkRrrC6ZKYDqdz-FZlWybDNCWDhB7MwlIgBm19cz6gav6GpKsI5dvSwNlvswX73AHD8XLU5Zl1o9U1b-hMezTOKPP-41WwqkNPvFQ5opVG_JrhiQkV_x211W9E47sxrwD3Or88ZgX-b-6g_UXuzzTxUV7IO9Ud5H2mqQ-AE6w4dhQ7fQyx_7Homy4YYtGn47ip0XrT-p1rdMZuaslRDQr2iWeIHUSym9foZHY8VkaTjtYpx_Xbala)](https://mermaid.live/edit#pako:eNqlkT1vwkAMhv-K5bGCsN_AQlN1oF9JpS63WDkDoeQuvXMEFeK_11FDAwtLt9M9j_y-so9YBcdoMPFXx77i-5rWkRrrC6ZKYDqdz-FZlWybDNCWDhB7MwlIgBm19cz6gav6GpKsI5dvSwNlvswX73AHD8XLU5Zl1o9U1b-hMezTOKPP-41WwqkNPvFQ5opVG_JrhiQkV_x211W9E47sxrwD3Or88ZgX-b-6g_UXuzzTxUV7IO9Ud5H2mqQ-AE6w4dhQ7fQyx_7Homy4YYtGn47ip0XrT-p1rdMZuaslRDQr2iWeIHUSym9foZHY8VkaTjtYpx_Xbala)

